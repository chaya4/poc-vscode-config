init-huskey:
	yarn add --dev  husky 
	yarn add --dev  @commitlint/cli 
	yarn add --dev  @commitlint/config-conventional 
	npx husky install
	npx husky-init && yarn


commitlint-gen-config:
	echo "{\n \"extends\":  [\"@commitlint/config-conventional\"] \n} " >.commitlintrc.json 
	echo "module.exports = {extends: ['@commitlint/config-conventional']}" > commitlint.config.js
	
golint:
	cd backend && golangci-lint run .
